﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CSDL
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        string database = CSDL.database();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack) return;
            try
            {
                string sql = "select * from LOAIHANG";
                SqlDataAdapter da = new SqlDataAdapter(sql, database);
                DataTable dt = new DataTable();
                da.Fill(dt);
                this.DataList1.DataSource = dt;
                this.DataList1.DataBind();

            }
            catch (SqlException ex)
            {
                Response.Write(ex.Message);
            }
        }

        protected void LinkButton1_Click1(object sender, EventArgs e)
        {
            string maloai = ((LinkButton)sender).CommandArgument;
            Context.Items["ml"] = maloai;
            Server.Transfer("sanpham.aspx");
        }

        protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
        {
            string ten = this.Login1.UserName;
            string matkhau = this.Login1.Password;
            string sql = "select * from KHACHHANG where TENDANGNHAP='" + ten + "' and MATKHAU = '" + matkhau + "'";

            DataTable table = new DataTable();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(sql, database);
                da.Fill(table);
            }
            catch (SqlException ex)
            {
                Response.Write(ex.Message);
            }

            if (table.Rows.Count != 0)
            {
                Response.Cookies["TENDANGNHAP"].Value = ten;
                Server.Transfer("sanpham.aspx");
            }
            else
            {
                this.Login1.FailureText = "Tên đăng nhập hay mật khẩu không đúng!";
            }
        }
    }
}