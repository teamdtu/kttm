﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace CSDL
{
    public partial class chitiet : System.Web.UI.Page
    {
        string database = CSDL.database();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack) return;
            string sql;
            if (Context.Items["mh"] == null)
            {
                sql = "select * from MATHANG";
            }
            else
            {
                string mahang = Context.Items["mh"].ToString();
                sql = "select * from MATHANG where MAHANG = '" + mahang + "'";

            }
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(sql, database);
                DataTable dt = new DataTable();
                da.Fill(dt);
                this.DataList1.DataSource = dt;
                this.DataList1.DataBind();
            }
            catch (SqlException ex)
            {
                Response.Write(ex.Message);
            }
        }
    }
}