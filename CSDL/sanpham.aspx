﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="sanpham.aspx.cs" Inherits="CSDL._1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Content1" runat="server">
    <asp:datalist runat="server" ID="DataList1" RepeatColumns="3">
        <ItemTemplate>
            <asp:Label ID="Label1" runat="server" Text='<%# Eval("TENHANG") %>'></asp:Label>
            <br />
            <asp:Image ID="Image1" runat="server" ImageUrl='<%# "~/image/" + Eval("HINH") %>' />
            <br />
            <asp:Label ID="Label2" runat="server" Text='<%# Eval("DONGIA", "{0:0.00} VND") %>'></asp:Label>
            <asp:LinkButton style="text-decoration:none; color:black; border:1px solid black; background-color:aqua;" ID="LinkButton2" runat="server" CommandArgument='<%# Eval("MAHANG") %>' OnClick="LinkButton2_Click">Chi Tiết</asp:LinkButton>
        </ItemTemplate>
    </asp:datalist>
</asp:Content>
