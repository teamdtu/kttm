﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace CSDL
{
    public partial class _1 : System.Web.UI.Page
    {
        string database = CSDL.database();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack) return;
            string sql;
            if (Context.Items["ml"] == null)
            {
                sql = "select * from MATHANG";
            }
            else
            {
                string maloai = Context.Items["ml"].ToString();
                sql = "select * from MATHANG where MALOAI = '" + maloai + "'";
            }
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(sql, database);
                DataTable dt = new DataTable();
                da.Fill(dt);
                this.DataList1.DataSource = dt;
                this.DataList1.DataBind();

            }
            catch (SqlException ex)
            {
                Response.Write(ex.Message);
            }
        }

        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            string mahang = ((LinkButton)sender).CommandArgument;
            Context.Items["mh"] = mahang;
            Server.Transfer("chitiet.aspx");
        }
    }
}